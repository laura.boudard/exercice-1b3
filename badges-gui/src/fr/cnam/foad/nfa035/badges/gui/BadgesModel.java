package fr.cnam.foad.nfa035.badges.gui;


import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import javax.swing.table.AbstractTableModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Modèle de table du composant JTable
 */
public class BadgesModel extends AbstractTableModel {

    private final String [] entetes = {"ID", "Code Série", "Début", "Fin", "Taille (octets"};
    private List<DigitalBadge> badges;
    private final long serialVersionUID = 4561898989L;
    DirectAccessBadgeWalletDAO dao;

    public BadgesModel () {


        try {
            dao = new DirectAccessBadgeWalletDAOImpl("badges-wallet/src/test/resources/" + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();
            badges = new ArrayList<>();
            badges.addAll(metaSet);
        } catch (IOException e) {
            e.printStackTrace();
        }

      }

    public List<DigitalBadge> getBadges() {
        return badges;
    }

    /**
     * JTable va utiliser cette méthode pour déterminer le nombre de lignes qu'il doit créer et afficher par défaut
     * @return le nombre de lignes du modèle
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return badges.size();
    }

    /**
     * Renvoie le nombre de colonnes du modèle. Un JTable utilise cette méthode
     * pour déterminer combien de colonnes il doit créer et afficher par défaut
     *
     * @return le nombre de colonne du modèle
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return entetes.length;
    }

    /**
     *
     * @param columnIndex
     * @return le nom des colonnes
     */
    @Override
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }

    /**
     * This empty implementation is provided so users don't have to implement
     * this method if their data model is not editable.
     *
     * @param aValue      value to assign to cell
     * @param rowIndex    row of cell
     * @param columnIndex column of cell
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        super.setValueAt(aValue, rowIndex, columnIndex);
    }

    /**
     *renvoie l'objet à l'intersection de la ligne et de la colonne
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0 :
                return badges.get(rowIndex).getMetadata().getBadgeId();
            case 1 :
                return badges.get(rowIndex).getSerial();
            case 2 :
                return badges.get(rowIndex).getBegin();
            case 3 :
                return badges.get(rowIndex).getEnd();
            case 4 :
                return badges.get(rowIndex).getMetadata().getImageSize();
            default:
                throw new IllegalArgumentException();
        }

    }
}
