package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class BadgeWalletGUI extends JFrame{

    private static final String RESOURCES_PATH = "badges-wallet/src/test/resources/";
    private JButton button_msg;
    private JPanel panelMain;
    private JTable table1;
    private JButton button1;


    public BadgeWalletGUI() {

        button1.addActionListener(new ActionListener() {
            /**
             * permet de faire une action quand on clique sur le bouton
             * @param e the event to be processed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,"Bien joué");
            }
        });

        DirectAccessBadgeWalletDAO dao;
        try {
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            TableModel tableModel = new BadgesModel();
            table1.setModel(tableModel);
            table1.setAutoCreateRowSorter(true);
            table1.setDefaultRenderer(dao.getWalletMetadata().getClass(), new BadgeSizeCellRenderer());
            table1.setDefaultRenderer(dao.getWalletMetadata().getClass(), new DefaultBadgeCellRenderer(Color.RED));
            table1.setDefaultRenderer(DigitalBadgeMetadata.class, new DefaultBadgeCellRenderer(Color.ORANGE));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Returns the image to be displayed as the icon for this frame.
     * <p>
     * This method is obsolete and kept for backward compatibility
     * only. Use {@link Window#getIconImages Window.getIconImages()} instead.
     * <p>
     * If a list of several images was specified as a Window's icon,
     * this method will return the first item of the list.
     *
     * @return the icon image for this frame, or {@code null}
     * if this frame doesn't have an icon image.
     * @see #setIconImage(Image)
     * @see Window#getIconImages()
     * @see Window#setIconImages
     */
    @Override
    public Image getIconImage() {
        return super.getIconImage();
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        JLabel label = new JLabel("My badge Wallet");
        frame.setContentPane(new BadgeWalletGUI().panelMain);
        frame.setIconImage(frame.getIconImage());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //calcul récursif des positions et des tailles
        frame.pack();
        //fait apparaitre la fenêtre
        frame.setSize(600,200);
        frame.setVisible(true);
    }
}
