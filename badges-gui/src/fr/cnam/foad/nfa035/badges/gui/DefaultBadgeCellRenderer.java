package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DefaultBadgeCellRenderer extends DefaultTableCellRenderer{

    Color originForeground = Color.WHITE;

    public DefaultBadgeCellRenderer(Color originForeground) {
        this.originForeground = originForeground;
        setOpaque(true);
    }


    /**
     * Returns the default table cell renderer.
     * <p>
     * During a printing operation, this method will be called with
     * <code>isSelected</code> and <code>hasFocus</code> values of
     * <code>false</code> to prevent selection and focus from appearing
     * in the printed output. To do other customization based on whether
     * or not the table is being printed, check the return value from
     * {@link JComponent#isPaintingForPrint()}.
     *
     * @param table      the <code>JTable</code>
     * @param value      the value to assign to the cell at
     *                   <code>[row, column]</code>
     * @param isSelected true if cell is selected
     * @param hasFocus   true if cell has focus
     * @param row        the row of the cell to render
     * @param column     the column of the cell to render
     * @return the default table cell renderer
     * @see JComponent#isPaintingForPrint()
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dateJour = new Date();
        Date dateEnd = (Date) value;
        setText(dateEnd.toString());
        if (dateEnd.after(dateJour)) {
            setForeground(Color.RED);
        }
        return this;
    }
}
